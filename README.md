# DDD Structure
[![Flask](https://img.shields.io/badge/Flask-v2.2.3-blue.svg)](https://pypi.org/project/Flask/)
![Python 3.10](https://img.shields.io/badge/Python-3.10-blue)

## Folder Structure

```
project
|           
+---modules
|   |   __init__.py
|   |   
|   \---core
|       |   __init__.py
|       |   
|       +---application
|       |   |   __init__.py
|       |   |   
|       |   +---services
|       |   |       __init__.py
|       |   |       
|       |   \---use_cases
|       |           __init__.py
|       |           
|       +---domain
|       |   |   entities.py
|       |   |   __init__.py
|       |   |   
|       |   +---models
|       |   |       __init__.py
|       |   |       
|       |   +---repositories
|       |   |       __init__.py
|       |   |       
|       |   \---services
|       |           __init__.py
|       |           
|       \---infrastructure
|           |   __init__.py
|           |   
|           +---repositories
|           |       __init__.py
|           |       
|           \---services
|                   __init__.py
|                   
+---shared_kernel
|   |   __init__.py
|   |   
|   +---domain
|   |       entity.py
|   |       exceptions.py
|   |       value_objects.py
|   |       __init__.py
|   |       
|   +---infrastructure
|   |       __init__.py
|   |       
|   \---utils
|           decorators.py
|           __init__.py
|           
+---tests
|   |   __init__.py
|   |   
|   +---modules
|   |   |   __init__.py
|   |   |   
|   |   \---core
|   |       |   __init__.py
|   |       |   
|   |       +---application
|   |       |   |   __init__.py
|   |       |   |   
|   |       |   +---services
|   |       |   |       __init__.py
|   |       |   |       
|   |       |   \---use_cases
|   |       |           __init__.py
|   |       |           
|   |       +---domain
|   |       |   |   entities
|   |       |   |   __init__.py
|   |       |   |   
|   |       |   +---models
|   |       |   |       __init__.py
|   |       |   |       
|   |       |   +---repositories
|   |       |   |       __init__.py
|   |       |   |       
|   |       |   \---services
|   |       |           __init__.py
|   |       |           
|   |       \---infrastructure
|   |           |   __init__.py
|   |           |   
|   |           +---repositories
|   |           |       __init__.py
|   |           |       
|   |           \---services
|   |                   __init__.py
|   |                   
|   \---shared_kernel
|       |   __init__.py
|       |   
|       +---domain
|       |       entity.py
|       |       exceptions.py
|       |       value_objects.py
|       |       __init__.py
|       |       
|       +---infrastructure
|       |       __init__.py
|       |       
|       \---utils
|               decorators.py
|               __init__.py
|   .gitignore
|   main.py
|   README.md
|   requirements.txt


```