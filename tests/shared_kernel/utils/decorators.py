def singleton(cls):
    instances = dict()

    def wrap(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]

    return wrap

def equality_by(*fields):
    def decorator(cls):
        def eq(self, other):
            if isinstance(other, cls):
                if fields:
                    return all(getattr(self, f) == getattr(other, f) for f in fields)
                else:
                    return all(getattr(self, attr) == getattr(other, attr) for attr in vars(self))
            return NotImplemented
        cls.__eq__ = eq
        return cls
    return decorator


def count_calls(func):
    def wrapper(*args, **kwargs):
        wrapper.count += 1
        return func(*args, **kwargs)
    wrapper.count = 0
    wrapper.__name__ = func.__name__
    return wrapper