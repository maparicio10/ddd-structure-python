from dataclasses import dataclass, field


@dataclass
class Entity:
    id: str = field(hash=True)